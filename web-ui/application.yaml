---
  kind: "Template"
  apiVersion: "v1"
  metadata:
    name: "cern-search-ui"
    creationTimestamp: null
    annotations:
      description: "Cern Search UI OpenShift template."
      tags: "cern-search-ui"
  labels:
    template: "cern-search-ui"
  objects:

    ###### PROXY ######
    -
      apiVersion: v1
      kind: Route
      metadata:
        labels:
          app: proxy
        name: web
      spec:
        to:
          kind: Service
          name: proxy
        port:
          targetPort: http
        tls:
          insecureEdgeTerminationPolicy: Redirect
          termination: edge
        selector:
          app: proxy
    -
      apiVersion: v1
      kind: Service
      metadata:
        name: proxy
        labels:
          app: proxy
      spec:
        ports:
        - name: 'http'
          port: 80
          targetPort: 8080
        selector:
          app: proxy
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        labels:
          app: proxy
        name: proxy
      spec:
        replicas: "${PROXY_REPLICAS}"
        template:
          metadata:
            name: proxy
            labels:
              app: proxy
          spec:
            containers:
            - name: proxy
              image: nginx
              env:
              - name: TZ
                value: "Europe/Zurich"
              ports:
              - containerPort: 8080
                protocol: TCP
              volumeMounts:
                - name: nginx-config
                  mountPath: /etc/nginx/conf.d
                - name: var-run
                  mountPath: /var/run
                - name: var-cache-nginx
                  mountPath: /var/cache/nginx
                - name: var-log-nginx
                  mountPath: /var/log/nginx
                - name: static
                  mountPath: /static
              readinessProbe:
                httpGet:
                  path: /ping
                  port: 8080
                initialDelaySeconds: 15
                timeoutSeconds: 1
            volumes:
              - name: nginx-config
                configMap:
                  defaultMode: 420
                  name: nginx-config
              - name: var-run
                emptyDir: {}
              - name: var-cache-nginx
                emptyDir: {}
              - name: var-log-nginx
                emptyDir: {}
              - name: static
                emptyDir: {}
      triggers:
        - type: ConfigChange
    ###### WEB APP (Web UI) ######
    -
      apiVersion: v1
      kind: Service
      metadata:
        name: web
        labels:
          app: web
          run: web
      spec:
        ports:
        - port: 3000
          protocol: TCP
        selector:
          app: web
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        labels:
          app: web
        name: web
      spec:
        replicas: "${WEB_REPLICAS}"
        revisionHistoryLimit: 10
        strategy:
          activeDeadlineSeconds: 21600
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        template:
          metadata:
            labels:
              app: web
          spec:
            containers:
            - name: web
              ports:
              - containerPort: 3000
                name: http
              env:
                - name: TZ
                  value: "Europe/Zurich"
              resources: 
                limits:
                  cpu: "1"
                  memory: 500Mi
                requests:
                  cpu: "100m"
                  memory: 100Mi
              readinessProbe:
                httpGet:
                  path: /
                  port: 3000
                  scheme: HTTP
                failureThreshold: 3
                initialDelaySeconds: 5
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 1
              livenessProbe:
                httpGet:
                  path: /
                  port: 3000
                  scheme: HTTP
                failureThreshold: 3
                initialDelaySeconds: 5
                periodSeconds: 10
                successThreshold: 1
                timeoutSeconds: 1
        triggers:
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - web
              from:
                kind: ImageStreamTag
                name: '${APPLICATION_IMAGE_NAME}:${APPLICATION_IMAGE_TAG}'
                namespace: "${TAGS_PROJECT}"
          - type: ConfigChange
    -
      apiVersion: autoscaling/v1
      kind: HorizontalPodAutoscaler
      metadata:
        labels:
          app: web
          template: cern-search-ui
        name: web
      spec:
        maxReplicas: "${MAX_WEB_REPLICAS}"
        minReplicas: "${WEB_REPLICAS}"
        scaleTargetRef:
          apiVersion: v1
          kind: DeploymentConfig
          name: web
        targetCPUUtilizationPercentage: "${SCALER_CPU_UTILIZATION}"

  parameters:
    - name: APPLICATION_IMAGE_NAME
      required: true
      value: "cern-search-ui"
    - name: APPLICATION_IMAGE_TAG
      value: "latest"
      required: true
    - name: PROXY_REPLICAS
      description: Number of proxy instances
      value: "1"
      required: true
    - name: WEB_REPLICAS
      description: Minimum number of web instances
      value: "1"
      required: true
    - name: MAX_WEB_REPLICAS
      description: Maximum number of web instances
      value: "2"
      required: true
    - name: SCALER_CPU_UTILIZATION
      description: Target CPU utilization to scale up/down the web pod
      value: "75"
      required: true
    - name: TAGS_PROJECT
      description: OpenShift project which contains all image versions.
      required: true
      value: "cern-search-master"
