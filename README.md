# CERN Search OpenShift template

This is a Fork of [Invenio-Openshift]. Therefore for full documentation refer to [Invenio template-openshift](https://gitlab.cern.ch/invenio/template-openshift).

You can find two folders:

- ``rest-api`` with the corresponding components to run a CERN Search as a Service API instance. With no Web UI.
- ``web-ui`` with the corresponding components to run the Web User Interface used for the global CERN Search.

## REST API Content
- ``application.yaml``:
  * Nginx proxy service and deployment configuration.
  * uWSGI web app service, deployment configuration and autoscaler.
  * celery workers service, deployment configuration and autoscaler.

- ``services.yaml``:
  * Redis cache service and deployment configuration.
  * RabbitMQ queue system service and deployment configuration.
  * PostgreSQL database service and deployment configuration.
  * Tika server service and deployment configuration.
  
- ``services-extra.yaml``:
  * Elasticsearch service and deployment configuration.
  
- ``storage.yaml``:
  * *records-files-claim* persistent volume claim used by the *cern-search-rest-api*  and  *worker* applications.

- ``configuration.yaml``:
  * ConfigMap for the *nginx.conf* configuration file.
  * ConfigMap for the *uwsgi.ini* configuration file.
  * ConfigMap for the *cern-search-rest-api* application.
  * ConfigMap for the *worker* application.
  * ConfigMap for the *redis.conf* configuration file.

- ``image_stream.yaml``:
  * ImageStream of the *cern-search-rest-api*.

- ``secrets.yaml``:
  * Secrets definition.

- ``routes.yaml``:
  * Routes definition.
  
Note that the services given in ``services-extra.yaml`` might not be suitable for a production deployment since they might require a more thorough configuration.

## Web UI Content
- ``application.yaml``:
  * Nginx proxy service and deployment configuration.
  * uWSGI web app service, deployment configuration and autoscaler.

- ``services.yaml``:
  * Redis cache service and deployment configuration.

- ``configuration.yaml``:
  * ConfigMap for the *nginx.conf* configuration file.
  * ConfigMap for the *uwsgi.ini* configuration file.
  * ConfigMap for the *cern-search-web-ui* application.

- ``image_stream.yaml``:
  * ImageStream of the *cern-search-web-ui*.

- ``job.yaml``:
  * Job configuration for background tasks.